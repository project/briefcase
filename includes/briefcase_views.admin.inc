<?php

/**
 * @file
 * Briefcase Views functions
 */


/**
 * "views_api" hook
 */
function briefcase_views_api() {
  return array(
    'api' => 2
  );
}


/**
 * Configuration page
 */
function briefcase_views_settings() {
  $form = array();
  
  // Views - Export Path
  $form['briefcase_views_export_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Views Export Path'),
    '#default_value'  => variable_get('briefcase_views_export_path', file_directory_path() .'/export/views'),
    '#required'       => TRUE,
    '#description'    => t('Destination path where exported Views files will be stored. Destination path <strong>must be WRITABLE</strong>.'),
  );
  
  // Views - Import Path
  $form['briefcase_views_import_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Views Import Path'),
    '#default_value'  => variable_get('briefcase_views_import_path', file_directory_path() .'/export/views'),
    '#required'       => TRUE,
    '#description'    => t('Source path from where you can import Views exported files.'),
  );
  
  // Views - Export File Extension
  $form['briefcase_views_extension'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Views exported file extension'),
    '#default_value'  => variable_get('briefcase_views_extension', '.views.php'),
    '#required'       => TRUE,
    '#description'    => t('Exported Views file extension. Default value: <em>.views.php</em>'),
  );
  
  // Views - Override option
  $form['briefcase_views_override'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Override existing Views?'),
    '#default_value'  => variable_get('briefcase_views_override', 1),
    '#required'       => FALSE,
    '#description'    => t('Override existing Views on import process.')
  );
  
  return system_settings_form($form);
}


/**
 * Configuration page form validation
 */
function briefcase_views_settings_validate($form, &$form_state) {
  // Check if destination path exists and it's writable
  if ( !empty($form_state['values']['briefcase_views_export_path']) ) {
    if (!file_check_directory($form_state['values']['briefcase_views_export_path'], FILE_CREATE_DIRECTORY) && !mkdir($form_state['values']['briefcase_views_export_path'], 0775, TRUE)) {
      form_set_error('briefcase_views_export_path', t('An automated attempt to create "%directory" directory failed, possibly due to a permissions problem.', array('%directory' => $form_state['values']['briefcase_views_export_path'])));
    }
    if ( ! is_writable($form_state['values']['briefcase_views_export_path']) ) {
      form_set_error('briefcase_views_export_path', t('The directory %directory is not writable.', array('%directory' => $form_state['values']['briefcase_views_export_path'])));
    }
  }
}


/**
 *  Export Views form page
 */
function briefcase_views_export_form() {
  // Start building form
  $form = array();
  
  // Get all VIEWs
  $vec_views = views_get_all_views(1);
  $vec_views_options = array();

  foreach ($vec_views as $view_name => $que_view) {
    if ( ! $que_view->disabled ) {
      $vec_views_options[$view_name] = $view_name;
    }
  }

  // Show all Views on checkbox list
  $form['export_views'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Views to export'),
    '#default_value' => array(),
    '#options'       => $vec_views_options,
    '#description'   => t('Select the views to export.'),
  );

  // Submit button
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Export'),
  );
  $form['submit']['#prefix']  = '<div class="help clear-block" style="margin: 30px auto 10px;">All the export content will be saved on <u>'. variable_get('briefcase_views_export_path', 'sites/all/export/views') .'</u>.';
  $form['submit']['#prefix'] .= ' If you want to choose another path, go to <a href="'. base_path() .'admin/settings/briefcase/views">Briefcase Views configuration page</a></div>';

  return $form;
}


/**
 *  Submit function for export View form page
 */
function briefcase_views_export_form_submit($form, &$form_state) {
  $vec_exported_views = array_filter(array_filter($form_state['values']['export_views']));
  if ( !empty($vec_exported_views) ) {
    // Export all selected Views
    $result_export = _briefcase_views_export($vec_exported_views);
    
    // Errors
    if ( !empty($result_export['errors']) ) {
      foreach ($result_export['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_export['messages']) ) {
      foreach ($result_export['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 *  Import Views form page
 */
function briefcase_views_import_form() {
  // Start building form
  $form = array();
  
  // Get all files saved on import folder
  $vec_files = _briefcase_get_files('views');
  $vec_views_options = array();
  
  if ( !empty($vec_files) ) {

    // Build checkox option list with files on directory
    foreach ($vec_files as $que_file) {
      $vec_views_options[$que_file->basename] = $que_file->filename;
    }
    
    // Show all Views on checkbox list
    $form['import_views'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Views to import'),
      '#default_value' => array(),
      '#options'       => $vec_views_options,
      '#description'   => t('Select the Views files to import.'),
    );

    // Submit button
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Import'),
    );
  }

  return $form;
}


/**
 *  Submit function for import View form page
 */
function briefcase_views_import_form_submit($form, &$form_state)  {
  $vec_imported_files = array_filter(array_filter($form_state['values']['import_views']));
  if ( !empty($vec_imported_files) ) {
    // Run Views import process
    $result_import = _briefcase_views_import($vec_imported_files);
    
    // Errors
    if ( !empty($result_import['errors']) ) {
      foreach ($result_import['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_import['messages']) ) {
      foreach ($result_import['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 * Theming Views export form page
 */
function theme_briefcase_views_export_form($form) {
  // Table header  
  $vec_header = array(
    theme('table_select_header_cell'),
    t('Name'),
    t('Export path'),
    t('Operations')
  );
    
  // Build table rows with Views information
  $vec_rows = array();
  if ( isset($form['export_views']['#options']) ) {
    foreach ( $form['export_views']['#options'] as $key_view => $val_view ) {
      // Remove title for checkbox
      $form['export_views'][$key_view]['#title'] = '';

      // Row data
      $vec_rows[] = array(
        drupal_render($form['export_views'][$key_view]),
        $key_view,
        _briefcase_path_corrections(variable_get('briefcase_views_export_path', file_directory_path() .'/export/views')) . $key_view . variable_get('briefcase_views_extension', '.views.php'),
        l(t('edit'), 'admin/build/views/edit/'. $key_view)
      );          
    }
  }
  
  // Render form with table structure
  return drupal_render($form['export_views']) . theme('table', $vec_header, $vec_rows) . drupal_render($form);
}


/**
 * Execute Views export process
 *
 * @access private 
 * @param array $vec_views
 *   Optional array with views to export. If empty, it will export all views
 * @return array
 */
function _briefcase_views_export($vec_views = array()) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Export ALL views?
  if ( empty($vec_views) ) {
    // Get all enabled Views (including default Views)
    $vec_views = views_get_all_views();
    foreach ($vec_views as $num_view => $que_view) {
      // Discard disabled Views
      if ( $que_view->disabled ) {
        unset($vec_views[$num_view]);
      }
    }
    
    // Only Views names is needed
    if ( !isset($vec_views[0]) ) {
      $vec_views = array_keys($vec_views);
    }
  }

  // Export all selected Views
  $num_exported = 0;
  foreach ( $vec_views as $view_name ) {
    $view = views_get_view($view_name);
    if ( !is_object($view) ) {
      $vec_result['errors'][] = t('View "@view": A view by that name doesn\'t exist.', array('@view' => $view_name) );
    }
    else {
      // Export method from Views class
      $output = $view->export();
    
      // Save output to file
      _briefcase_save_content($view_name, $output, 'views');
    
      // Message
      $vec_result['messages'][] = t('View "@name" has been exported.', array('@name' => $view_name) ); 
      
      // Counting correct exported views
      $num_exported++;
    }
  }
  
  // Final message
  $vec_result['messages'][] = t('A total of @count Views have been exported', array('@count' => $num_exported) );
  
  return $vec_result;
}


/**
 * Execute Views import process
 *
 * @access private 
 * @param array $vec_files
 * @return array
 */
function _briefcase_views_import($vec_files) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Initialize Views module
  module_load_all();
  views_include('view');
  views_include_handlers();

  // Import all selected Views
  foreach ($vec_files as $views_file) {
    // Execute Views import code from content file
    $view_content = _briefcase_get_content($views_file, 'views');
    eval($view_content);
    
    // Validate functions (from "views_ui_import_validate" function)
    if ( !is_object($view) ) {
      $vec_result['errors'][] = t('Unable to interpret view code from file "@filename".', array('@filename' => $views_file));
      return $vec_result;
    }
    
    // Check if view exists
    $existing_view = views_get_view($view->name);
    $views_replaced = FALSE;
    if ($existing_view && $existing_view->type != t('Default')) {
      $override_views = variable_get('briefcase_views_override', 1);
      if ( $override_views == 1 ) {
        // Delete old view
        $existing_view->delete();
        views_object_cache_clear('view', $view->name);
        $views_replaced = TRUE;
      }
      else {
        $vec_result['errors'][] = t('View "@view": A view by that name already exists; please choose a different name', array('@view' => $view->name) );
        return $vec_result;         
      }
    }
    
    $view->init_display();
    
    $broken = FALSE;

    // Make sure that all plugins and handlers needed by this view actually exist.
    foreach ($view->display as $id => $display) {
      if (empty($display->handler) || !empty($display->handler->broken)) {
        $vec_result['errors'][] = t('Display plugin @plugin is not available.', array('@plugin' => $display->display_plugin) );
        $broken = TRUE;
        continue;
      }

      $plugin = views_get_plugin('style', $display->handler->get_option('style_plugin'));
      if (!$plugin) {
        $vec_result['errors'][] = t('Style plugin @plugin is not available.', array('@plugin' => $display->handler->get_option('style_plugin') ) );
        $broken = TRUE;
      }
      elseif ($plugin->uses_row_plugin()) {
        $plugin = views_get_plugin('row', $display->handler->get_option('row_plugin'));
        if (!$plugin) {
          $vec_result['errors'][] = t('Row plugin @plugin is not available.', array('@plugin' => $display->handler->get_option('row_plugin') ) );
          $broken = TRUE;
        }
      }

      foreach (views_object_types() as $type => $info) {
        $handlers = $display->handler->get_handlers($type);
        if ($handlers) {
          foreach ($handlers as $id => $handler) {
            if ($handler->broken()) {
              $vec_result['errors'][] = t('@type handler @table.@field is not available.', array(
                '@type' => $info['stitle'],
                '@table' => $handler->table,
                '@field' => $handler->field,
              ) );
              $broken = TRUE;
            }
          }
        }
      }
    }

    if ($broken) {
      $vec_result['errors'][] = t('Unable to import view.');
      return $vec_result;
    }

    // Save new view
    $view->save();
    menu_rebuild();
    cache_clear_all('*', 'cache_views');
    cache_clear_all();
    views_object_cache_clear('view', $view->name);
  
    // Store in cache and then go to edit.
    views_ui_cache_set($view);
    
    // Message
    if ( $views_replaced ) {
      $vec_result['messages'][] = t('View @name has been overrided and imported.', array('@name' => $view->name) );
    }
    else {
      $vec_result['messages'][] = t('View @name has been imported.', array('@name' => $view->name) );
    }
  }
  
  return $vec_result;
}
