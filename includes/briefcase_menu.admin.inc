<?php

/**
 * @file
 * Briefcase Menus functions
 */


/**
 * Configuration page
 */
function briefcase_menu_settings() {
  $form = array();
  
  // Menus - Export Path
  $form['briefcase_menu_export_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Menus Export Path'),
    '#default_value'  => variable_get('briefcase_menu_export_path', file_directory_path() .'/export/menu'),
    '#required'       => TRUE,
    '#description'    => t('Destination path where exported Menus files will be stored. Destination path <strong>must be WRITABLE</strong>.'),
  );
  
  // Menus - Import Path
  $form['briefcase_menu_import_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Menus Import Path'),
    '#default_value'  => variable_get('briefcase_menu_import_path', file_directory_path() .'/export/menu'),
    '#required'       => TRUE,
    '#description'    => t('Source path from where you can import Menus exported files.'),
  );
  
  // Menus - Export File Extension
  $form['briefcase_menu_extension'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Menus exported file extension'),
    '#default_value'  => variable_get('briefcase_menu_extension', '.menu.php'),
    '#required'       => TRUE,
    '#description'    => t('Exported Menus file extension. Default value: <em>.menu.php</em>'),
  );
  
  return system_settings_form($form);
}


/**
 * Configuration page form validation
 */
function briefcase_menu_settings_validate($form, &$form_state) {
  // Check if destination path exists and it's writable
  if ( !empty($form_state['values']['briefcase_menu_export_path']) ) {
    if ( !file_check_directory($form_state['values']['briefcase_menu_export_path'], FILE_CREATE_DIRECTORY) && !mkdir($form_state['values']['briefcase_menu_export_path'], 0775, TRUE)) {
      form_set_error('briefcase_menu_export_path', t('An automated attempt to create "%directory" directory failed, possibly due to a permissions problem.', array('%directory' => $form_state['values']['briefcase_menu_export_path'])));
    }
    if ( !is_writable($form_state['values']['briefcase_menu_export_path']) ) {
      form_set_error('briefcase_menu_export_path', t('The directory %directory is not writable.', array('%directory' => $form_state['values']['briefcase_menu_export_path'])));
    }
  }
}


/**
 *  Export Menus form page
 */
function briefcase_menu_export_form() {
  // Start building form
  $form = array();
  
  // Get all MENUs
  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
  $vec_menu_options = array();
  while ($menu = db_fetch_array($result)) {
    $vec_menu_options[$menu['menu_name']] = $menu['title'];
  }
  // Show all Menus on checkbox list
  $form['export_menu'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Menus to export'),
    '#default_value' => array(),
    '#options'       => $vec_menu_options,
    '#description'   => t('Select the menus to export.'),
  );

  // Submit button
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Export'),
  );
  $form['submit']['#prefix']  = '<div class="help clear-block" style="margin: 30px auto 10px;">All the export content will be saved on <u>'. variable_get('briefcase_menu_export_path', 'sites/all/export/menu') .'</u>.';
  $form['submit']['#prefix'] .= ' If you want to choose another path, go to <a href="'. base_path() .'admin/settings/briefcase/menu">Briefcase Menus configuration page</a></div>';

  return $form;
}


/**
 *  Submit function for export Menu form page
 */
function briefcase_menu_export_form_submit($form, &$form_state) {
  $vec_exported_menu = array_filter(array_filter($form_state['values']['export_menu']));
  if ( !empty($vec_exported_menu) ) {
    // Export all selected Menus
    $result_export = _briefcase_menu_export($vec_exported_menu);
    
    // Errors
    if ( !empty($result_export['errors']) ) {
      foreach ($result_export['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_export['messages']) ) {
      foreach ($result_export['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 *  Import Menus form page
 */
function briefcase_menu_import_form() {
  // Start building form
  $form = array();
  
  // Get all files saved on import folder
  $vec_files = _briefcase_get_files('menu');
  $vec_menu_options = array();
  
  if ( !empty($vec_files) ) {

    // Build checkox option list with files on directory
    foreach ($vec_files as $que_file) {
      $vec_menu_options[$que_file->basename] = $que_file->filename;
    }
    
    // Show all Menus on checkbox list
    $form['import_menu'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Menus to import'),
      '#default_value' => array(),
      '#options'       => $vec_menu_options,
      '#description'   => t('Select the Menus files to import.'),
    );

    // Submit button
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Import'),
    );
  }

  return $form;
}


/**
 *  Submit function for import Menu form page
 */
function briefcase_menu_import_form_submit($form, &$form_state)  {
  $vec_imported_files = array_filter(array_filter($form_state['values']['import_menu']));
  if ( !empty($vec_imported_files) ) {
    // Run Menus import process
    $result_import = _briefcase_menu_import($vec_imported_files);
    
    // Errors
    if ( !empty($result_import['errors']) ) {
      foreach ($result_import['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_import['messages']) ) {
      foreach ($result_import['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 * Theming Menus export form page
 */
function theme_briefcase_menu_export_form($form) {
  // Table header  
  $vec_header = array(
    theme('table_select_header_cell'),
    t('Title'),
    t('Name'),
    t('Export path'),
    t('Operations')
  );
    
  // Build table rows with Content Type information
  $vec_rows = array();
  if ( isset($form['export_menu']['#options']) ) {
    foreach ( $form['export_menu']['#options'] as $key_menu => $val_menu ) {
      // Remove title for checkbox
      $form['export_menu'][$key_menu]['#title'] = '';

      // Row data
      $vec_rows[] = array(
        drupal_render($form['export_menu'][$key_menu]),
        $val_menu,
        $key_menu,
        _briefcase_path_corrections(variable_get('briefcase_menu_export_path', file_directory_path() .'/export/menu')) . $key_menu . variable_get('briefcase_menu_extension', '.menu.php'),
        l(t('edit'), 'admin/build/menu-customize/'. $key_menu)
      );          
    }
  }
  
  // Render form with table structure
  return drupal_render($form['export_menu']) . theme('table', $vec_header, $vec_rows) . drupal_render($form);
}


/**
 * Execute Menus export process
 *
 * @access private 
 * @param array $vec_menu
 *   Optional array with menus to export. If empty, it will export all menus
 * @return array
 */
function _briefcase_menu_export($vec_menu = array()) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Export ALL menus?
  if ( empty($vec_menu) ) {
    $result = db_query("SELECT menu_name FROM {menu_custom} ORDER BY title");
    while ($menu = db_fetch_array($result)) {
      $vec_menu[$menu['menu_name']] = $menu['menu_name'];
    }
  }

  // Only Menus names is needed
  if ( !isset($vec_menu[0]) ) {
    $vec_menu = array_keys($vec_menu);
  }

  // Export all selected Menus
  global $menu_admin;
  $num_exported = 0;

  foreach ( $vec_menu as $menu_name ) {
    // Firt of all, check if menu exists to be exported
    if ( ! _briefcase_menu_exists_menu($menu_name) ) {
      $vec_result['errors'][] = t('Menu "@menu": A menu by that name doesn\'t exist.', array('@menu' => $menu_name) );
    }
    else {  
      // Get menu info
      $vec_menu_info = db_fetch_array(db_query("SELECT * FROM {menu_custom} WHERE menu_name = '%s'", $menu_name));
        
      // Get menu tree data
      $sql = "SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
              FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
              WHERE ml.menu_name = '%s'
              ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";
      $result = db_query($sql, $menu_name);
      $tree = menu_tree_data($result);
      $node_links = array();
      menu_tree_collect_node_links($tree, $node_links);

      // We indicate that a menu administrator is running the menu access check. 
      $menu_admin = TRUE;
      menu_tree_check_access($tree, $node_links);
      $menu_admin = FALSE;

      // Let's build export code
      $vec_menu_items = _briefcase_menu_get_menu($tree);

      $output = "\$menu = array();\n";
      $output .= "\$menu['". $menu_name ."'] = array(); \n";
      $output .= "\$menu['". $menu_name ."']['menu'] = ". var_export($vec_menu_info, TRUE) .";\n";
      $output .= "\$menu['". $menu_name ."']['items'] = ". var_export($vec_menu_items, TRUE) .";\n";
    
      // Save output to file
      _briefcase_save_content($menu_name, $output, 'menu');

      // Message
      $vec_result['messages'][] = t('Menu "@name" has been exported.', array('@name' => $menu_name) ); 

      // Counting correct exported menus
      $num_exported++;
    }
  }
  
  // Final message
  $vec_result['messages'][] = t('A total of @count Menus have been exported', array('@count' => $num_exported) );
  
  return $vec_result;
}


/**
 * Execute Menus import process
 *
 * @access private 
 * @param array $vec_files
 * @return array
 */
function _briefcase_menu_import($vec_files) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Import all selected Menus
  foreach ($vec_files as $menu_file) {
    // Execute Menus import code from content file
    $menu_content = _briefcase_get_content($menu_file, 'menu');
    eval($menu_content);
    
    // Validate function
    if ( !is_array($menu) ) {
      $vec_result['errors'][] = t('Unable to interpret menu code from file "@filename".', array('@filename' => $menu_file));
      return $vec_result;
    }

    $vec_mlids = array();
    foreach ( $menu as $menu_name => $vec_menu ) {
      // Check if menu exists
      $exists_menu = _briefcase_menu_exists_menu($menu_name);

      // Menu no exists -> CREATE a new menu
      if ( !$exists_menu ) {
        $path = 'admin/build/menu-customize/';
        // Add 'menu-' to the menu name to help avoid name-space conflicts.
        $menu['menu_name'] = 'menu-'. $vec_menu['menu']['menu_name'];
        $link['link_title'] = $vec_menu['menu']['title'];
        $link['link_path'] = $path . $vec_menu['menu']['menu_name'];
        $link['router_path'] = $path .'%';
        $link['module'] = 'menu';
        $link['plid'] = db_result(db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s' AND module = '%s'", 'admin/build/menu', 'system'));
        menu_link_save($link);
        if ( !db_query("INSERT INTO {menu_custom} (menu_name, title, description) VALUES ('%s', '%s', '%s')", $vec_menu['menu']['menu_name'], $vec_menu['menu']['title'], $vec_menu['menu']['description']) ) {
          $vec_result['errors'][] = t('Unable to create a new menu: "%title".', array('%title' => $vec_menu['menu']['title']));
          return $vec_result;
        }
        // Message
        $vec_result['messages'][] = t('The menu %title has been created.', array('%title' => $vec_menu['menu']['title']) );
      }
      
      // Menu exists -> UPDATE information
      else {
        $path = 'admin/build/menu-customize/';
        if ( !db_query("UPDATE {menu_custom} SET title = '%s', description = '%s' WHERE menu_name = '%s'", $vec_menu['menu']['title'], $vec_menu['menu']['description'], $vec_menu['menu']['menu_name']) ) {
          $vec_result['errors'][] = t('Unable to update the menu "%title".', array('%title' => $vec_menu['menu']['title']));
          return $vec_result;
        }
        $result = db_query("SELECT mlid FROM {menu_links} WHERE link_path = '%s'", $path . $vec_menu['menu']['menu_name']);
        while ($m = db_fetch_array($result)) {
          $link = menu_link_load($m['mlid']);
          $link['link_title'] = $vec_menu['menu']['title'];
          menu_link_save($link);
        }
        // Message
        $vec_result['messages'][] = t('The menu %title has been updated.', array('%title' => $vec_menu['menu']['title']) );
        /*
        // Option - Delete all menu tree data
        if ( !in_array($menu_name, menu_list_system_menus()) ) {
          // Delete all menu data
          _briefcase_menu_delete_menu($menu_name);
        }     
        */
      }

      // Menu items
      if ( !empty($vec_menu['items']) ) {
        
        // Keep track of the new mlids per depth and parent.
        $vec_depth_mlids = array();
        
       foreach ($vec_menu['items'] as $item) {
          // Check if menu item already exists
          if ( $exists_menu ) { 
            $existing_item = _briefcase_menu_get_item_menu($item['menu_name'], $item['link_path']);
            // Menu item exists. Keep same mlid
            if ( $existing_item ) {
              $item['mlid'] = $existing_item['mlid'];
            }
          }

          // Item with a parent, so keep track of the original parent and the new mlid belonging to it
          if (intval($item['depth']) > 1 && !isset($vec_depth_mlids[$item['depth']][$item['plid']])) {
            $item['plid'] = $vec_depth_mlids[$item['depth']][$item['plid']] = $new_mlid;
          }
          // Item with the same parent as we have already processed before
          elseif (intval($item['depth']) > 1) {
            $item['plid'] = $vec_depth_mlids[$item['depth']][$item['plid']];
          }

          // Save link menu
          $new_mlid = menu_link_save($item);
        
          if (module_exists('i18nmenu')) {
            // Ensure we have a menu item to work with.
            // patch from http://drupal.org/node/786230#comment-2911038
            $item['parent'] = $item['menu_name'] .':'. $item['plid'];
            _i18nmenu_update_item($item);
          }
        }
      }
    }   
  }
  
  return $vec_result;
}


/**
 * Recursive helper function. Returns an array with menu tree structure
 */
function _briefcase_menu_get_menu($tree) {
  $vec_menu = array();
  foreach ($tree as $data) {
    $item = $data['link'];
    if ( $item ) {
      $item_key = 'mlid-'. $item['mlid'];
      $vec_menu[$item_key] = $item;
      if ( !empty($data['below']) ) {
        $vec_menu_below = _briefcase_menu_get_menu($data['below']);
        if ( !empty($vec_menu_below) ) {
          foreach ($vec_menu_below as $key_menu_below => $val_menu_below) {
            $vec_menu[$key_menu_below] = $val_menu_below;
          }
        }
      } 
    }
  }
  return $vec_menu;
}


/**
 * Checks if a menu exists
 */
function _briefcase_menu_exists_menu($menu_name) {
  return ( db_result(db_query("SELECT menu_name FROM {menu_custom} WHERE menu_name = '%s'", $menu_name)) || db_result(db_query_range("SELECT menu_name FROM {menu_links} WHERE menu_name = '%s'", $menu_name, 0, 1)) );
}

/**
 * Returns a item link from a menu
 */
function _briefcase_menu_get_item_menu($menu_name, $link_path) {
  $result = db_query("SELECT * FROM {menu_links} WHERE menu_name = '%s' AND link_path = '%s'", $menu_name, $link_path);
  while ($link = db_fetch_array($result)) {
    return $link;
  }
  return FALSE; 
}


/**
 * Delete a Drupal menu and all its item.
 * Fragment code from Drupal core menu module (modules/menu/menu.admin.inc -> function menu_delete_menu_confirm_submit() )
 */
function _briefcase_menu_delete_menu($menu_name) {
  // Reset all the menu links defined by the system via hook_menu.
  $result = db_query("SELECT * FROM {menu_links} ml INNER JOIN {menu_router} m ON ml.router_path = m.path WHERE ml.menu_name = '%s' AND ml.module = 'system' ORDER BY m.number_parts ASC", $menu_name);
  while ($item = db_fetch_array($result)) {
    menu_reset_item($item);
  }

  // Delete all links to the overview page for this menu.
  $result = db_query("SELECT mlid FROM {menu_links} ml WHERE ml.link_path = '%s'", 'admin/build/menu-customize/'. $menu_name);
  while ($m = db_fetch_array($result)) {
     menu_link_delete($m['mlid']);
  }

  // Delete all the links in the menu and the menu from the list of custom menus.
  db_query("DELETE FROM {menu_links} WHERE menu_name = '%s'", $menu_name);
  db_query("DELETE FROM {menu_custom} WHERE menu_name = '%s'", $menu_name);
  // Delete all the blocks for this menu.
  db_query("DELETE FROM {blocks} WHERE module = 'menu' AND delta = '%s'", $menu_name);
  db_query("DELETE FROM {blocks_roles} WHERE module = 'menu' AND delta = '%s'", $menu_name);
  menu_cache_clear_all();
  cache_clear_all();
  $t_args = array('%title' => $$menu_name);
  drupal_set_message(t('The custom menu %title has been deleted.', $t_args));
  watchdog('menu', 'Deleted custom menu %title and all its menu items.', $t_args, WATCHDOG_NOTICE);
  return TRUE;
}
