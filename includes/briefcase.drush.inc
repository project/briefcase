<?php


/**
 * @file
 * Drush commands for Briefcase
 */
 
/**
 * Implementation of hook_drush_command().
 */
function briefcase_drush_command() {
  // Commad "briefcase"
  $items['briefcase'] = array(
    'callback'    => 'briefcase_drush_list',
    'description' => 'Show a list of exported elements',
    'arguments'   => array(
      'type' => "List type: menu, views, cck or imagecache"
    ),
  );
  
  // Command "briefcase export"
  $items['briefcase-export'] = array(
    'callback'    => 'briefcase_drush_export',
    'description' => 'Export one or more elements (Views, CCK, ...) to files.',
    'examples'    =>  array(
      'briefcase export views last_content_view' => 'Export "last_content_view" to a file.' 
    ),
    'arguments' => array(
      'type'  => "Export type: menu, views, cck or imagecache",
      'items' => "Optional. A specific item to export. It could be a comma-separated list. For example: cck1,cck2,cck3"
    ),
  );
  
  // Command "briefcase import"
  $items['briefcase-import'] = array(
    'callback'    => 'briefcase_drush_import',
    'description' => 'Import one or more Views files to Drupal database.',
    'examples'    => array(
      'briefcase views import last_content_view' => 'Import "last_content_view" filen export to Drupal database.' 
    ),
    'arguments' => array(
      'type'  => "Import type: menu, views, cck or imagecache",
      'items' => "Optional. A specific item to import. It could be a comma-separated list. For example: cck1,cck2,cck3"
    ),
  );

  return $items;
}


/**
 * Implementation of hook_drush_help().
 */
function briefcase_drush_help($section) {
  switch ($section) {
    case 'drush:briefcase':
      return dt("Show Briefcase information.");
    break;
    
    case 'drush:briefcase-export':
      return dt("Export one or more elements (Views, CCK, Imagecache) into files.");
    break;
    
    case 'drush:briefcase-import':
      return dt('Import one or more elements (Views, CCK, Imagecache) from files to Drupal.');
    break;
  }
}


/**
 * Briefcase info page
 */
function briefcase_drush_list($list_type='') {  
  // Only accepts allowed types
  $vec_allowed_types = array('menu', 'cck', 'views', 'imagecache');
  
  if ( !empty($list_type) ) {
    if ( in_array($list_type, $vec_allowed_types) ) {
      $list_type = array($list_type);
    }
  }
  else {
    $list_type = $vec_allowed_types;
  }
  
  
  if ( !empty($list_type) ) {
    // Table with Briefcase Type Info
    $vec_type_rows = array( array(
      dt('Type'),
      dt('Export Path'),
      dt('Import Path'),
      dt('File extension')
    ));
    
    // Table with Files Information
    $vec_file_rows = array( array(
      dt('Type'),
      dt('Name'),
      dt('Date')
    ));
    
    foreach ( $list_type as $que_type ) {
      $type_label = $que_type == 'cck' ? 'CCK' : ucfirst($que_type);

      $vec_type_rows[] = array(
        $type_label,
        variable_get('briefcase_'. $que_type .'_export_path', 'sites/all/export/'. $que_type),
        variable_get('briefcase_'. $que_type .'_import_path', 'sites/all/export/'. $que_type),
        variable_get('briefcase_'. $que_type .'_extension', '.'. $que_type .'.php')
      );
        
      // Get all files saved on import folder
      $vec_files = _briefcase_get_files($que_type);
      
      if ( !empty($vec_files) ) {
        // Build list with files on directory
        foreach ($vec_files as $que_file) {
          $vec_file_rows[] = array(
            $type_label,
            $que_file->basename,
            format_date(fileatime($que_file->filename), 'small')
          );
        }
      }
    }
  }

  // Title
  drush_print("\n". dt('## BRIEFCASE INFORMATION ##') ."\n");

  // Table with Briefcase Type Info
  drush_print('-- '. dt('Briefcase Types Info') .' --');
  drush_print_table($vec_type_rows, TRUE);
  drush_print();
  
  // Table with Files Information
  drush_print('-- '. dt('Files List') .' --');
  drush_print_table($vec_file_rows, TRUE);
  drush_print();
  
  // Drush commands
  drush_print('-- '. dt('Drush Commands') .' --');
  drush_print(dt('Available commands:'));
  drush_print(dt(' drush briefcase [menu | views | cck | imagecache]'));
  drush_print(dt(' drush briefcase-export [menu | views | cck | imagecache] <items>'));
  drush_print(dt(' drush briefcase-import [menu | views | cck | imagecache] <items>'));
   
  drush_print(_briefcase_help_examples('all'));

  return TRUE;
}


/**
 * Export Drush function
 */
function briefcase_drush_export($export_type='', $items='') {
  // Supported modules (export types)
  $vec_types = array('menu', 'views', 'cck', 'imagecache');

  // Must set a type to export
  if ( empty($export_type) OR !in_array($export_type, $vec_types) ) {
    drush_print( dt('ERROR: You must select a correct type to export.') );
    drush_print( dt('Usage: drush briefcase export [menu | views | cck | imagecache] <items>"') );
    drush_print( _briefcase_help_examples('export') );
    return FALSE;
  }
  
  // Export type label (name showed at screen)
  $export_type_label == 'cck' ? 'CCK' : ucfirst($export_type);
    
  // No items have been selected -> Export ALL files (confirm before)
  if ( empty($items) ) {
    if ( !drush_confirm( dt("Do you really want to export ALL the @type?", array('@type' => $export_type ) ) ) ) {
      return FALSE;
    }
    $items = array();
  }
  else {
    // Get item/s to export
    if ( preg_match('/,/', $items) ) {
      $vec_items = explode(',', $items);
    }
    else {
      $vec_items = array($items);
    }
  }
  
  switch ( $export_type ) {    
    // Export Content Types
    case 'cck':
      // Get all CCK Types
      $vec_types = node_get_types('names');
      
      // Set which CCK we want to export
      if ( empty($vec_items) ) {
        $vec_items = $vec_types;
      }
      
      // First, one check&clean step
      else {
        // We need to pass an associative array as argument of _briefcase_cck_export()
        $vec_exported_items = array();
        foreach ( $vec_items as $num_item => $que_item ) {
          // Check if Content Type exists
          if ( isset($vec_types[$que_item]) ) {
            $vec_exported_items[$que_item] = $vec_types[$que_item];
          }
          else {
            drush_print( dt('CCK "@cck": A content type by that name doesn\'t exist.', array('@cck' => $que_item) ) );
          }
        }
        $vec_items = $vec_exported_items;
      }
      
      // Run CCK export process
      module_load_include('module', 'briefcase', 'briefcase');
      module_load_include('inc', 'briefcase', 'includes/briefcase_cck.admin');
      $result_export = _briefcase_cck_export($vec_items);
    break;
   
    // Export other types: views, menu and imagecache
    case 'views':
    case 'menu':
    default:
      // Run export process
      module_load_include('module', 'briefcase', 'briefcase');
      module_load_include('inc', 'briefcase', 'includes/briefcase_'. $export_type .'.admin');
      $function_export = '_briefcase_'. $export_type .'_export';
      $result_export = $function_export($vec_items);
    break;
  }
  
  // Let's show all results
  if ( !empty($result_export) ) {
    // Errors
    if ( !empty($result_export['errors']) ) {
      foreach ($result_export['errors'] as $que_error) {
        drush_print( dt("Error @type Export: @error", array('@type' => $export_type_label, '@error' => $que_error) ));
      }
    }

    // Messages
    if ( !empty($result_export['messages']) ) {
      foreach ($result_export['messages'] as $que_message) {
        drush_print($que_message);
      }
    }
  }
  
  return;
}


/**
 * Import Drush function
 */
function briefcase_drush_import($import_type='', $items='') {
  // Supported modules (import types)
  $vec_types = array('views', 'cck', 'imagecache', 'menu');
  
  // Must set a type to import
  if ( empty($import_type) OR !in_array($import_type, $vec_types) ) {
    drush_print( dt('ERROR: You must select a correct type to import.') );
    drush_print( dt('Usage: drush briefcase import [views | cck | imagecache] <items>"') );
    drush_print( _briefcase_help_examples('import') );
    return FALSE;
  }
  
  // Import type label (name showed at screen)
  $import_type_label == 'cck' ? 'CCK' : ucfirst($import_type);
    
  // No items have been selected -> Import ALL items (confirm before)
  $vec_files = _briefcase_get_files($import_type);
  if ( empty($items) ) {
    if ( !drush_confirm( dt("Do you really want to import ALL (@total) @type files?", array('@type' => $import_type, '@total' => count($vec_files) ) ) ) ) {
      return FALSE;
    }
    
    // Import all files on import directory
    $vec_items = array_keys($vec_files);
    $import_path = variable_get('briefcase_' . $import_type .'_import_path', 'sites/all/export/' . $import_type);
    foreach ($vec_items as $num_item => $que_item) {
      $vec_items[$num_item] = str_replace($import_path . '/', '', $que_item);
    }
  }
  
  // Selected items
  else {
    // Get item/s to import
    if ( preg_match('/,/', $items) ) {
      $vec_items = explode(',', $items);
    }
    else {
      $vec_items = array($items);
    }
    
    // Look up imported files
    $vec_errors = array();
    $import_path = variable_get('briefcase_' . $import_type . '_import_path', 'sites/all/export/' . $import_type);
    $import_extension = variable_get('briefcase_' . $import_type .'_extension', '.' . $import_type . '.php');
    foreach ($vec_items as $num_item => $que_item) {
      // Check if item file exists
      $item_file = $que_item . $import_extension;
      if ( !isset($vec_files[$import_path .'/'. $item_file]) ) {
        $vec_errors[] = dt("ERROR: File \"@filename\" doesn't exists on @filepath.", array('@filename' => $item_file, '@filepath' => $import_path) );
      }
      $vec_items[$num_item] .= $import_extension;
    }

    // One or more files haven't been found
    if ( !empty($vec_errors) ) {
      foreach ($vec_errors as $que_error) {
        drush_print($que_error);
      }
      drush_print( dt("Import process could not be done. \n") );
      return FALSE;
    }
  }
  
  // If we've arrived here, we can start to import
  switch ( $import_type ) {
    // Export other types: views, menu and imagecache
    case 'views':
    case 'cck':
    default:
      // Run import process
      module_load_include('module', 'briefcase', 'briefcase');
      module_load_include('inc', 'briefcase', 'includes/briefcase_'. $import_type .'.admin');
      $function_export = '_briefcase_'. $import_type .'_import';
      $result_import = $function_export($vec_items);
    break;
  }
  
  if ( !empty($result_import) ) {
    // Errors
    if ( !empty($result_import['errors']) ) {
      foreach ($result_import['errors'] as $que_error) {
        drush_print( dt("Error @type Import: @error", array('@type' => $import_type_label, '@error' => $que_error) ));
      }
    }

    // Messages
    if ( !empty($result_import['messages']) ) {
      foreach ($result_import['messages'] as $que_message) {
        drush_print( strtr($que_message, array('<em>' => '', '</em>' => '') ) );
      }
    }
  }
  
  return;
}


/**
 * Private function that shows examples for commands
 */
function _briefcase_help_examples($command, $hide_head=FALSE) {
  // Show title?
  if ( !$hide_head ) {
    $output  = "\nSome examples: \n";
  }
  
  // Help for commands showing examples
  switch ( $command ) {
    case 'export':
      $output .= "\n -- Export all enabled Views into files\n";
      $output .= " drush briefcase-export views\n\n";
      $output .= " -- Export 'last_users' View into a file\n";
      $output .= " drush briefcase-export views last_users\n\n";    
      $output .= " -- Export Page and Story types\n";
      $output .= " drush briefcase-export cck page,story\n";
    break;
    
    case 'import':
      $output .= "\n -- Import all exported Views files\n";
      $output .= " drush briefcase-import views\n\n";
      $output .= " -- Import 'last_users' View to Drupal\n";
      $output .= " drush briefcase-import views last_users\n\n";
      $output .= " -- Import Page and News types\n";
      $output .= " drush briefcase-import cck page,news\n";
    break;
    
    default:
      $output .= " -- Briefcase info for Views (including files)\n";
      $output .= " drush briefcase views\n";
      $output .= _briefcase_help_examples('export', TRUE);
      $output .= _briefcase_help_examples('import', TRUE);
    break;
  }
  
  return dt($output);
}
