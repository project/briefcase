<?php

/**
 * @file
 * Briefcase CCK functions
 */

/**
 * Configuration page form
 */
function briefcase_cck_settings() {
  $form = array();
  
  // CCK - Export Path
  $form['briefcase_cck_export_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('CCK - Export Path'),
    '#default_value'  => variable_get('briefcase_cck_export_path', file_directory_path() .'/export/cck'),
    '#required'       => TRUE,
    '#description'    => t('Destination path where exported CCK files will be stored. Destination path <strong>must be WRITABLE</strong>.'),
  );
  
  // CCK - Import Path
  $form['briefcase_cck_import_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('CCK - Import Path'),
    '#default_value'  => variable_get('briefcase_cck_import_path', file_directory_path() .'/export/cck'),
    '#required'       => TRUE,
    '#description'    => t('Source path from where you can import CCK exported files.'),
  );
  
  // CCK - Export File Extension
  $form['briefcase_cck_extension'] = array(
    '#type'           => 'textfield',
    '#title'          => t('CCK - Exported File Extension'),
    '#default_value'  => variable_get('briefcase_cck_extension', '.cck.php'),
    '#required'       => TRUE,
    '#description'    => t('Exported CCK file extension. Default value: <em>.cck.php</em>'),
  );

  return system_settings_form($form);
}


/**
 * Configuration page form validation
 */
function briefcase_cck_settings_validate($form, &$form_state) {
  // Check if destination path exists and it's writable
  if ( !empty($form_state['values']['briefcase_cck_export_path']) ) {
    if (!file_check_directory($form_state['values']['briefcase_cck_export_path'], FILE_CREATE_DIRECTORY) && !mkdir($form_state['values']['briefcase_cck_export_path'], 0775, TRUE)) {
      form_set_error('briefcase_cck_export_path', t('An automated attempt to create "%directory" directory failed, possibly due to a permissions problem.', array('%directory' => $form_state['values']['briefcase_cck_export_path'])));
    }
    if ( ! is_writable($form_state['values']['briefcase_cck_export_path']) ) {
      form_set_error('briefcase_cck_export_path', t('The directory %directory is not writable.', array('%directory' => $form_state['values']['briefcase_cck_export_path'])));
    }
  }
}

    
/**
 * Export CCK form page
 */
function briefcase_cck_export_form() {
  // Start building form
  $form = array();
  
  // Get all CCK Types
  $types = node_get_types('names');

  // Show all Node Types on checkbox list
  $form['export_cck'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Content types to export'),
    '#default_value' => array(),
    '#options'       => array_map('check_plain', $types),
    '#description'   => t('Select the content types to export.'),
  );
  
  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );
  $form['submit']['#prefix']  = '<div class="help clear-block" style="margin: 30px auto 10px;">All the export content will be saved on <u>'. variable_get('briefcase_cck_export_path', 'sites/all/export/cck') .'</u>.';
  $form['submit']['#prefix'] .= ' If you want to choose another path, go to <a href="'. base_path() .'admin/settings/briefcase/cck">Briefcase CCK configuration page</a></div>';

  return $form;
}


/**
 * Submit function for export CCK form page
 *
 * Process the export, get field admin forms for all requested fields
 * and save the form values as formatted text.
 */
function briefcase_cck_export_form_submit($form, &$form_state)  {
  $vec_exported_cck = array_filter($form_state['values']['export_cck']);
  if ( !empty($vec_exported_cck) ) {
    // Export all selected CCK
    $result_export = _briefcase_cck_export($vec_exported_cck);
    
    // Errors
    if ( !empty($result_export['errors']) ) {
      foreach ($result_export['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_export['messages']) ) {
      foreach ($result_export['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
  return TRUE;
}


/**
 *  Import CCK form page
 */
function briefcase_cck_import_form() {
  // Start building form
  $form = array();
  
  // Get all files saved on import folder
  $vec_files = _briefcase_get_files('cck');
  $vec_cck_options = array();
  
  if ( !empty($vec_files) ) {

    // Build checkox option list with files on directory
    foreach ($vec_files as $que_file) {
      $vec_cck_options[$que_file->basename] = $que_file->filename;
    }

    // Show all Node Types on checkbox list
    $form['import_cck'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Content types to import'),
      '#default_value' => array(),
      '#options'       => $vec_cck_options,
      '#description'   => t('Select the Content Types files to import.'),
    );

    // Submit button
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Import'),
    );
  }

  return $form;
}


/**
 *  Submit function for import CCK form page
 */
function briefcase_cck_import_form_submit($form, &$form_state)  {
  $vec_imported_files = array_filter(array_filter($form_state['values']['import_cck']));
  if ( !empty($vec_imported_files) ) {
    // Run CCK import process
    $result_import = _briefcase_cck_import($vec_imported_files);
    
    // Errors
    if ( !empty($result_import['errors']) ) {
      foreach ($result_import['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_import['messages']) ) {
      foreach ($result_import['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 * Theming CCK export form page
 */
function theme_briefcase_cck_export_form($form) {
  // Table header  
  $vec_header = array(
    theme('table_select_header_cell'),
    t('Name'),
    t('Type'),
    t('Export path'),
    t('Operations')
  );
    
  // Build table rows with Content Type information
  $vec_rows = array();
  if ( isset($form['export_cck']['#options']) ) {
    foreach ( $form['export_cck']['#options'] as $key_type => $val_type ) {
      // Remove title for checkbox
      $form['export_cck'][$key_type]['#title'] = '';

      // Row data
      $vec_rows[] = array(
        drupal_render($form['export_cck'][$key_type]),
        $val_type,
        $key_type,
        _briefcase_path_corrections(variable_get('briefcase_cck_export_path', file_directory_path() .'/export/cck')) . $key_type . variable_get('briefcase_cck_extension', '.cck.php'),
        l(t('edit'), 'admin/content/node-type/'. $key_type)
      );          
    }
  }
  
  // Render form with table structure
  return drupal_render($form['export_cck']) . theme('table', $vec_header, $vec_rows) . drupal_render($form);
}


/**
 * Execute CCK export process
 *
 * @access private 
 * @param array $vec_cck
 *   Optional array with CCK to export. If empty, it will export all CCK on database
 * @return array
 */
function _briefcase_cck_export($vec_cck = array()) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );
  
  // Include files from Content module
  require_once './includes/common.inc';
  module_load_include('inc', 'node', 'content_types');
  module_load_include('inc', 'content', 'includes/content.admin');
  module_load_include('inc', 'content', 'includes/content.crud'); 
  
  // Export all selected CCK types
  $num_exported = 0;
  foreach ( $vec_cck as $type_name => $type_human_name ) {     
    // CCK info
    $vec_info = array(
      'type'    => content_types($type_name),
      'fields'  => array()
    );
    
    // Data we need to export (works like "content_copy_export" function arguments)
    $vec_export = array(
      'name'    => $type_name,
      'fields'  => array_keys($vec_info['type']['fields'])
    );
          
    // Groups
    if ( module_exists('fieldgroup') ) {
      $vec_info['groups'] = fieldgroup_groups($type_name);
      $vec_export['groups'] = array_keys($vec_info['groups']);
    }
      
    // Array with output content
    // ----------------> $vec_output = array('type' =>   content_types($type_name) );

    //--> Based on "content_copy_export" function (content_copy.module)
    
    // Get the content type info by submitting the content type form.
    $node_state = array('values' => array('type_name' => $type_name));
    drupal_execute('node_type_form', $node_state, node_get_types('type', $type_name));

    // Get an array of groups to export.
    // Record a macro for each group by submitting the group edit form.
    $groups = array();
    if ( !empty($vec_export['groups']) ) {
      // ----------------> $vec_output['groups'] = fieldgroup_groups($type_name);
      foreach ( $vec_export['groups'] as $group ) {
        $group_state = array('values' => array('group_name' => $group));
        drupal_execute('fieldgroup_group_edit_form', $group_state, $type_name, $group, 'edit');
      }
    }

    // Get an array of fields to export
    // Record a macro for each field by submitting the field settings form.
    // Omit fields from the export if their module is not currently installed
    // otherwise the system will generate errors when the macro tries to execute their forms.
    if ( !empty($vec_export['fields']) ) {
      foreach ( $vec_export['fields'] as $field_name ) {
        $field = $vec_info['type']['fields'][$field_name];
        $field_types = _content_field_types();
        $field_module = $field_types[$field['type']]['module'];
        $widget_types = _content_widget_types();
        $widget_module = $widget_types[$field['widget']['type']]['module'];
        if (!empty($field_module) && module_exists($field_module) && !empty($widget_module) && module_exists($widget_module)) {
          $field_state = array('values' => content_field_instance_collapse($field));
          $field_state['values']['op'] = t('Save field settings');
          if ( module_exists('fieldgroup') ) {
            // Avoid undefined index error by always creating this.
            $field_state['values']['group'] = '';
            $group_name = fieldgroup_get_group($type_name, $field_name);
            if (in_array($group_name, $groups)) {
              $field_state['values']['group'] = $group_name;
            }
          }
          drupal_execute('content_field_edit_form', $field_state, $type_name, $field_name);
        }

        $query = 'SELECT display_settings FROM {'. content_instance_tablename() .'} WHERE field_name = \'%s\'';
        $row_info = db_fetch_array(db_query($query, $field_name));
        if ( $display_settings = unserialize($row_info['display_settings']) ) {
          $vec_field = content_fields($field_name, $type_name);
          $vec_field['display_settings'] = $display_settings;

          // Copy group fields
          if ( module_exists('fieldgroup') AND !empty($field_state['values']['group']) ) {
            $vec_field['group'] = $field_state['values']['group'];
          }
        }
         $vec_info['fields'][] = $vec_field;
      }
    }

    $output = '';
    foreach ( $vec_info as $form_type => $form ) {
      $output .= "\$content['$form_type']  = ". var_export((array) $form, TRUE) .";\n";
    }

    // Add weights of non-CCK fields.
    if ( $extra = variable_get('content_extra_weights_'. $type_name, array()) ) {
      $output .= "\$content['extra']  = ". var_export((array) $extra, TRUE) .";\n";
    }
    
    // Save output to file
    _briefcase_save_content($type_name, $output, 'cck');
    
    // Message
    $vec_result['messages'][] = t('CCK "@name" has been exported.', array('@name' => $type_name) ); 
    
    // Counting correct exported CCK
    $num_exported++;  
  }
  
  // Final message
  $vec_result['messages'][] = t('A total of @count CCK have been exported', array('@count' => $num_exported) );
  
  return $vec_result;
}


/**
 * Execute CCK import process
 *
 * @access private 
 * @param array $vec_files
 * @return array
 */
function _briefcase_cck_import($vec_files) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );
  
  // Include Content & Node module functions  
  require_once './includes/common.inc';
  include_once( drupal_get_path('module', 'node') .'/content_types.inc');
  include_once( drupal_get_path('module', 'content') .'/includes/content.admin.inc');
  module_load_include('inc', 'content', 'includes/content.crud');
  
  // Import all selected CCKs
  foreach ( $vec_files as $cck_file ) {
    // Get the content variable from CCK imported file
    $cck_content = _briefcase_get_content($cck_file, 'cck');
    
    // Header text that appears in all messages
    $message_header = t('File "@file":', array('@file' => $cck_file)) . ' ';
    
    $content = NULL;
    // Convert the import formatted text back into a $content array.
    // Return if errors generated or not an array.
    // Use '@' to suppress errors about undefined constants in the macro.
    @eval($cck_content);
  
    // Preliminary error trapping, must have valid arrays to work with.
    if ( !isset($content) || !isset($content['type']) || !is_array($content) || !is_array($content['type']) ) {
      $vec_result['errors'][] = $message_header . t('The import data is not valid import text.');
      return $vec_result;
    }
    
    // Get all type and field info for this database.
    $content_info = _content_type_info();

    $imported_type = $content['type'];
    $imported_type_name = $imported_type['type'];
    $imported_type_label = $imported_type['name'];    

    // It is allowed to import a type with no fields,
    // so the fields array could be empty and must be cast as an array.
    $imported_fields = isset($content['fields']) ? $content['fields'] : array();

    // Perform more pre-import error trapping.
    // If there are potential problems, exit without doing the import.
    $not_enabled = array();

    // The groups array could be empty and still valid, make sure to cast it as an array.
    // If there are groups in the import, make sure the fieldgroup module is enabled.
    $imported_groups = array();
    if ( isset($content['groups']) && module_exists('fieldgroup') ) {
      $imported_groups = (array) $content['groups'];
    }
    elseif ( isset($content['groups']) && is_array($content['groups']) ) {
      $not_enabled[] = 'fieldgroup';
    }

    // Make sure that all the field and widget modules in the import are enabled in this database.
    foreach ( $imported_fields as $import ) {
      $field = content_field_instance_collapse($import);
      if (empty($field['module']) || empty($field['widget_module'])) {
        $not_enabled[] = $field['field_name'];
      }
      else {
        if ( !module_exists($field['module']) ) {
          $not_enabled[] = $field['module'];
        }
        if ( !module_exists($field['widget_module']) ) {
          $not_enabled[] = $field['widget_module'];
        }
      }
    }

    // If any required module is not enabled, set an error message and exit.
    if ( $not_enabled ) {
      $vec_result['errors'][] = $message_header . t('The following modules must be enabled for this import to work: %modules.', array('%modules' => implode(', ', array_unique($not_enabled))) );
      return $vec_result;
    }

    // Make sure the imported content type doesn't already exist in the database.
    $vec_types = node_get_types('names');
    $create_new_type = FALSE;
    if ( !in_array($imported_type_name, array_keys($content_info['content types'])) ) {
      $create_new_type = TRUE;
    }

    // Create the content type, if requested.
    if ( $create_new_type ) {
      $type = (object) $imported_type;
      $values = $imported_type;
      // Prevent a warning in node/content_types.inc
      $type->has_title = TRUE;
      $type_form_state = array('values' => $values);

      // There's no API for creating node types, we still have to use drupal_execute().
      drupal_execute('node_type_form', $type_form_state, $type);

      // Reset type and database values once new type has been added.
      $type_name  = $imported_type_name;
      $type_label = node_get_types('name', $type_name);
      content_clear_type_cache();
      $content_info = _content_type_info();

      if ( form_get_errors() || !isset($content_info['content types']) || !is_array($content_info['content types'][$type_name]) ) {
        $vec_result['errors'][] = t('An error has occurred adding the content type %type.<br/>Please check the errors displayed for more details.', array('%type' => $imported_type_name) );
        return $vec_result;
      }
    }
    else {
      $type_name  = $imported_type_name;
      $type_label = $imported_type_label;
    }

    // Create the groups for this type, if they don't already exist.
    if ( module_exists('fieldgroup') && $imported_groups ) {
      foreach ( $imported_groups as $group ) {
        $group_name = $group['group_name'];
        fieldgroup_save_group($type_name, $group);
      }
      // Reset the static variable in fieldgroup_groups() with new data.
      fieldgroup_groups('', FALSE, TRUE);
    }

    // Iterate through the field forms in the import and execute each.
    $rebuild = FALSE;
    foreach ( $imported_fields as $field ) {
      // Make sure the field doesn't already exist in the type.
      // If so, do nothing, fields can't be duplicated within a content type.
      $field_name   = $field['field_name'];

      // Might need to overwrite the content type name if a new type was created.
      $field['type_name'] = $type_name;

      if ( !empty($field['field_name']) && isset($content_info['content types'][$type_name]['fields'][$field_name]) ) {
        $vec_result['messages'][] = t('The imported field %field_label (%field_name) was not added to %type because that field already exists in %type.', array(
          '%field_label'  =>  $field['label'],
          '%field_name'   =>  $field_name,
          '%type'     =>  $type_label
        ));
      }
      else {
        $field = content_field_instance_create($field, FALSE);
        $rebuild = TRUE;
        $vec_result['messages'][] = t('The field %field_label (%field_name) was added to the content type %type.', array(
          '%field_label'  =>  $field['widget']['label'],
          '%field_name'   =>  $field_name,
          '%type'     =>  $type_label
        ));
      }

      // Fieldgroup module erases all group related data when a module that
      // provides a content type is disabled, but CCK does not remove the fields.
      // In this case, we should ensure group data related to fields is properly
      // restored. Hence, we need to update field group data for newly imported
      // field, but also for fields that already exist.
      if (module_exists('fieldgroup') && isset($imported_groups)) {
        fieldgroup_update_fields($field);
      }
    }

    // Clear caches and rebuild menu only if any field has been created.
    if ($rebuild) {
      content_clear_type_cache(TRUE);
      menu_rebuild();
    }

    // Import weights of non-CCK fields.
    if ( isset($content['extra']) ) {
      variable_set('content_extra_weights_'. $type_name, $content['extra']);
    }
  }
  
  return $vec_result;
}

