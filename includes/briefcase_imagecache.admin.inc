<?php

/**
 * @file
 * Briefcase Imagecache functions
 */

/**
 * Configuration page form
 */
function briefcase_imagecache_settings() {
  $form = array();
  
  // Imagecache - Export Path
  $form['briefcase_imagecache_export_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Imagecache - Export Path'),
    '#default_value'  => variable_get('briefcase_imagecache_export_path', file_directory_path() .'/export/imagecache'),
    '#required'       => TRUE,
    '#description'    => t('Destination path where exported Imagecache files will be stored. Destination path <strong>must be WRITABLE</strong>.'),
  );
  
  // Imagecache - Import Path
  $form['briefcase_imagecache_import_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Imagecache - Import Path'),
    '#default_value'  => variable_get('briefcase_imagecache_import_path', file_directory_path() .'/export/imagecache'),
    '#required'       => TRUE,
    '#description'    => t('Source path from where you can import Imagecache exported files.'),
  );
  
  // Imagecache - Export File Extension
  $form['briefcase_imagecache_extension'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Imagecache - Exported File Extension'),
    '#default_value'  => variable_get('briefcase_imagecache_extension', '.imagecache.php'),
    '#required'       => TRUE,
    '#description'    => t('Exported Imagecache file extension. Default value: <em>.imagecache.php</em>'),
  );

  return system_settings_form($form);
}


/**
 * Configuration page form validation
 */
function briefcase_imagecache_settings_validate($form, &$form_state) {
  // Check if destination path exists and it's writable
  if ( !empty($form_state['values']['briefcase_imagecache_export_path']) ) {
    if (!file_check_directory($form_state['values']['briefcase_imagecache_export_path'], FILE_CREATE_DIRECTORY) && !mkdir($form_state['values']['briefcase_imagecache_export_path'], 0775, TRUE)) {
      form_set_error('briefcase_imagecache_export_path', t('An automated attempt to create "%directory" directory failed, possibly due to a permissions problem.', array('%directory' => $form_state['values']['briefcase_imagecache_export_path'])));
    }
    if ( ! is_writable($form_state['values']['briefcase_imagecache_export_path']) ) {
      form_set_error('briefcase_imagecache_export_path', t('The directory %directory is not writable.', array('%directory' => $form_state['values']['briefcase_imagecache_export_path'])));
    }
  }
}


/**
 * Export Imagecache form page
 */
function briefcase_imagecache_export_form() {
    
  // Start building form
  $form = array();

  // Get all Presets
  $vec_presets = imagecache_presets();
  $vec_preset_options = array();

  foreach ($vec_presets as $que_preset) {
    $vec_preset_options[$que_preset['presetid']] = $que_preset['presetname'];
  }

  // Show all Presets on checkbox list
  $form['export_imagecache'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Presets to export'),
    '#default_value' => array(),
    '#options'       => $vec_preset_options,
    '#description'   => t('Select the Imagecache presets to export.'),
  );
  
  // Submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );
  $form['submit']['#prefix']  = '<div class="help clear-block" style="margin: 30px auto 10px;">All the export content will be saved on <u>'. variable_get('briefcase_cck_imagecache_path', 'sites/all/export/imagecache') .'</u>.';
  $form['submit']['#prefix'] .= ' If you want to choose another path, go to <a href="'. base_path() .'admin/settings/briefcase/imagecache">Briefcase Imagecache configuration page</a></div>';

  return $form;
}


/**
 *  Submit function for export Imagecache presets form page
 */
function briefcase_imagecache_export_form_submit($form, &$form_state) {
  $vec_exported_imagecache = array_filter(array_filter($form_state['values']['export_imagecache']));
  if ( !empty($vec_exported_imagecache) ) {
    // Export all selected presets
    $result_export = _briefcase_imagecache_export($vec_exported_imagecache);
    
    // Errors
    if ( !empty($result_export['errors']) ) {
      foreach ($result_export['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_export['messages']) ) {
      foreach ($result_export['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 *  Import Imagecache form page
 */
function briefcase_imagecache_import_form() {
  // Start building form
  $form = array();
  
  // Get all files saved on import folder
  $vec_files = _briefcase_get_files('imagecache');
  $vec_imagecache_options = array();
  
  if ( !empty($vec_files) ) {

    // Build checkox option list with files on directory
    foreach ($vec_files as $que_file) {
      $vec_imagecache_options[$que_file->basename] = $que_file->filename;
    }
    
    // Show all Imagecache on checkbox list
    $form['import_imagecache'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Imagecache to import'),
      '#default_value' => array(),
      '#options'       => $vec_imagecache_options,
      '#description'   => t('Select the Imagecache preset files to import.'),
    );

    // Submit button
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Import'),
    );
  }

  return $form;
}


/**
 *  Submit function for import View form page
 */
function briefcase_imagecache_import_form_submit($form, &$form_state)  {
  $vec_imported_files = array_filter(array_filter($form_state['values']['import_imagecache']));
  if ( !empty($vec_imported_files) ) {
    // Run Imagecache import process
    $result_import = _briefcase_imagecache_import($vec_imported_files);
    
    // Errors
    if ( !empty($result_import['errors']) ) {
      foreach ($result_import['errors'] as $que_error) {
        drupal_set_message($que_error, 'error');
      }
    }
    
    // Messages
    if ( !empty($result_import['messages']) ) {
      foreach ($result_import['messages'] as $que_message) {
        drupal_set_message($que_message);
      }
    }
  }
}


/**
 * Theming Imagecache export form page
 */
function theme_briefcase_imagecache_export_form($form) {
  // Table header  
  $vec_header = array(
    theme('table_select_header_cell'),
    t('Name'),
    t('Export path'),
    t('Operations')
  );
    
  // Build table rows with Imagecache information
  $vec_rows = array();
  if ( isset($form['export_imagecache']['#options']) ) {
    foreach ( $form['export_imagecache']['#options'] as $id_preset => $name_preset ) {
      // Remove title for checkbox
      $form['export_imagecache'][$id_preset]['#title'] = '';

      // Row data
      $vec_rows[] = array(
        drupal_render($form['export_imagecache'][$id_preset]),
        $name_preset,
        _briefcase_path_corrections(variable_get('briefcase_imagecache_export_path', file_directory_path() .'/export/imagecache')) . strtolower($name_preset) . variable_get('briefcase_imagecache_extension', '.imagecache.php'),
        l(t('edit'), 'admin/build/imagecache/'. $id_preset)
      );          
    }
  }
  
  // Render form with table structure
  return drupal_render($form['export_imagecache']) . theme('table', $vec_header, $vec_rows) . drupal_render($form);
}


/**
 * Execute Imagecache export process
 *
 * @access private 
 * @param array $vec_presets
 *   Optional array with imagecache to export. If empty, it will export all imagecache
 * @return array
 */
function _briefcase_imagecache_export($vec_presets = array()) {

  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Export ALL imagecache presets?
  if ( empty($vec_presets) ) {
    // Get all Imagecache presets
    $vec_presets = imagecache_presets();
  }

  // Export all selected Views
  $num_exported = 0;
  foreach ( $vec_presets as $id_preset ) {
      // Load preset info
    if ( !is_numeric($id_preset) ) {
       $preset = imagecache_preset_by_name($id_preset);
    }
    else {
      $preset = imagecache_preset($id_preset);
    }

    // Firt of all, check if preset exists to be exported
    if ( empty($preset) ) {
      $vec_result['errors'][] = t('Preset "@preset": A preset by that name doesn\'t exist.', array('@preset' => $id_preset) );
    }
    else {
      // Export method from imagecache_ui_preset_export_form() function
      if (isset($preset['presetid'])) {
        unset($preset['presetid']);
      }
      if (isset($preset['storage'])) {
        unset($preset['storage']);
      }
      foreach ($preset['actions'] as $id => $action) {
        unset($preset['actions'][$id]['actionid']);
        unset($preset['actions'][$id]['presetid']);
      }
      $output = '$presets = array();';
      $output .= "\n";
      $output .= '$presets[\''. $preset['presetname'] .'\'] = ';
      $output .= var_export($preset, TRUE) .';';

      // Save output to file
      _briefcase_save_content(strtolower($preset['presetname']), $output, 'imagecache');

      // Message
      $vec_result['messages'][] = t('Imagecache preset "@name" has been exported.', array('@name' => $preset['presetname']) ); 

      // Counting correct exported imagecache
      $num_exported++;
    }
  }
  
  // Final message
  $vec_result['messages'][] = t('A total of @count Imagecache presets have been exported', array('@count' => $num_exported) );
  
  return $vec_result;
}


/**
 * Execute Imagecache import process
 *
 * @access private 
 * @param array $vec_files
 * @return array
 */
function _briefcase_imagecache_import($vec_files) {
  // Array with results (errors and messages)
  $vec_result = array(
    'errors'  =>  array(),
    'messages'  =>  array() 
  );

  // Import all selected Imagecache presets
  foreach ($vec_files as $imagecache_file) {
    // Execute Imagecache import code from content file
    $presets_content = _briefcase_get_content($imagecache_file, 'imagecache');
    eval($presets_content);
    
    if ( !isset($presets) OR !is_array($presets) ) {
      $vec_result['errors'][] = t('Unable to interpret imagecache code from file "@filename".', array('@filename' => $imagecache_file));
      return $vec_result;
    }
    
    $vec_presets = array();
    $vec_actions = array();
    foreach ( $presets as $que_preset ) {
      if ( isset($que_preset['presetname']) ) {
        $vec_presets[] = array(
          'presetname' => $que_preset['presetname']
        );
        $vec_actions[$que_preset['presetname']] = $que_preset['actions'];
      }
    }
    
    if ( empty($vec_presets) ) {
      $vec_result['errors'][] = t('Unable to interpret imagecache code from file "@filename".', array('@filename' => $imagecache_file));
      return $vec_result;   
    }
    
    foreach ( $vec_presets as $que_preset ) {
      // Check if preset exists
      $existing_preset = imagecache_preset_by_name($que_preset['presetname']);

      // Override existing preset
      if ( !empty($existing_preset) ) {
        // Get preset information
        $preset = imagecache_preset_save($existing_preset); 
        $vec_existing_actions = imagecache_preset_actions($preset);
        
        // Delete all preset actions (override)
        if ( !empty($vec_existing_actions) ) {
          foreach ( $vec_existing_actions as $que_action ) {
            imagecache_action_delete($que_action);
          }
        }
      }
      // Create new preset
      else {
        $preset = imagecache_preset_save($que_preset);  
      }

      if ( isset($vec_actions[$que_preset['presetname']]) ) {
        foreach ( $vec_actions[$que_preset['presetname']] as $action ) {
          if ( empty($action['presetid']) ) {
            $action['presetid'] = $preset['presetid'];
          }
          imagecache_action_save($action);
        }
      }
      // Message
      $vec_result['messages'][] = t('Imagecache preset @name has been imported.', array('@name' => $que_preset['presetname']) );
    }       
  }
  
  return $vec_result;
}
