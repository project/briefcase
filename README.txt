

CONTENTS
========
* OVERVIEW
* INSTALLATION
* REQUIREMENTS
* EXPORT FILES & FOLDERS
* DRUSH USAGE
* CREDITS


OVERVIEW
========
This module allows you to export and import exportables elements from
other modules (CCK, Views, Imagecache, ...) in a quick and clean way.

Quick because you need just one click (or command) to export and import
all your exportables you want.

Clean because every exportable will be saved to a SEPARATED FILE in the
TARGET PATH and FILE EXTENSION you choose.

Briefcase module is very powerful if you combine it with Subversion
or GIT. You can save every modification for every exportable element
and track version changes of them.


INSTALLATION
============
Install process works like other Drupal Modules.
  * Put the module in your drupal modules directory and enable it.
  * Configure settings at admin/settings/briefcase
  * Go to admin/content/briefcase to make exports or imports.


REQUIREMENTS
============
* CCK and Content Copy.
  http://drupal.org/project/cck

* Views and Views Export.
  http://drupal.org/project/views



EXPORT FILES & FOLDERS
======================
When you export one or more elements with Briefcase...

  1. A NEW FILE will be created with following name structure:
     "<exportable_element>.<briefcase_extension>"
      where...
        <briefcase_extesion> is a value you can edit on Briefcase
        configuration page

  2. This file is stored into FOLDER you've chosen on Briefcase
     configuration page. By default, sites/all/export/<exportable_type>
     
For example, if you export a View called "top_ten_news", Briefcase will
save export data content to: "sites/all/export/views/top_ten_new.views.php"

You can change file extension and target folder for every exportable 
element (Views, CCK, ...). To do it, go to "admin/settings/briefcase".


DRUSH USAGE
===========
Briefcase provide the following Drush commands:

drush briefcase [views | cck]

  Show Briefcase configuration settings and show a list with all
  the files you've exported or you can import.

  Examples:
    "drush briefcase"        >> Briefcase info for all exportables.      
    "drush briefcase views"  >> Briefcase info for Views (with files).
  

drush briefcase export [views | cck] <items>

  Export one or more elements (Views, CCK, Imagecache) into files.

  Examples:
    "drush briefcase export views"           >> Export all enabled Views into files.
    "drush briefcase export views my_view"   >> Export 'my_view' View into a file.
    "drush briefcase export cck page,story"  >> Export Page and Story content types.


drush briefcase import [views | cck] <items>

  Import one or more elements (Views, CCK, Imagecache) from files
  to your Drupal system.
  
  Examples:
	"drush briefcase import views"          >> Import all exported Views files.
    "drush briefcase import views my_view"  >> Import 'last_users' View to Drupal.   
    "drush briefcase import cck page,news"  >> Import Page and News content types.


CREDITS
=======
This module has been sponsored by Strabinarius.
  - http://www.strabinarius.com
  - http://drupal.org/profile/companies/Strabinarius

Authors:
  - Fabián Ruiz (http://drupal.org/user/719376)
  - Daniel Canet (http://drupal.org/user/273841)
